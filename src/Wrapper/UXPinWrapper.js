import React from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import grey from "@material-ui/core/colors/grey";
import Playfairwoff from '../Fonts/PlayfairDisplay-Regular.woff';
import Playfairwoff2 from '../Fonts/PlayfairDisplay-Regular.woff2';
import Montserratwoff from '../Fonts/Montserrat-Thin.woff';
import Montserratwoff2 from '../Fonts/Montserrat-Thin.woff2';



const theme = createMuiTheme({

  palette: {
    primary: {
       main: '#000000',
       "&:hover": {
        color: "#ffffff",
        backgroundColor: "#000000"
      }
       },
    secondary: { main: '#9e9e9e' }
  },

  typography: {
    fontSize: '16px',
    fontFamily: {

      playfair400: {
        fontFamily: 'Playfair Display',
        fontStyle: 'normal',
        fontDisplay: 'swap',
        fontWeight: 400,
        src: `local('Playfair Display'), 
              url(${Playfairwoff}) format('woff'),
              url(${Playfairwoff2}) format('woff2')`,
        sunicodeRange: 'U+0-FF, U+131, U+152-153, U+2BB-2BC, U+2C6, U+2DA, U+2DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD'
      },

      montserrat100: {
        fontFamily: 'Montserrat',
        fontStyle: 'thin',
        fontDisplay: 'swap',
        fontWeight: 100,
        src: `local('Montserrat'), 
              url(${Montserratwoff}) format('woff'),
              url(${Montserratwoff2}) format('woff2')`,
        sunicodeRange: 'U+0-FF, U+131, U+152-153, U+2BB-2BC, U+2C6, U+2DA, U+2DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD'
      }
  }

  },

  overrides: {

    MuiTypography: {
      colorPrimary: {
        color: grey[900]
      },
      colorSecondary: {
        color: "#9e9e9e"
      },
      colorTextPrimary: {
        color: grey[900]
      },
      colorTextSecondary: {
        color: "#757575"
      },
      h1: {
        fontSize: '26px'
      },
      h2: {
        fontSize: "24px",
      },
      h3: {
        fontSize: '22px'
      },
      subtitle1: {
        fontSize: "22px"
      },
      subtitel2: {
        fontSize: "19px"
      },
      body1: {
        fontSize: "22px"
      },
      body2: {
        fontSize: "19px"
      },
      body: {
        fontSize: "16px"
      },
      regular: {
        fontWeight:  'normal'
      },
      bold: {
        fontWeight: 'bold'
      },
      lighter: {
        fontWeight: 'lighter'
      }
    },

    MuiLink: {
      root: {
        fontWeight: "bold",
      },
      underlineHover: {
        textDecoration: "none",
        '&:hover': {
          textDecoration: "none",
          borderBottom: "3px solid #000000"
        },
      },
      underlineAlways: {
        textDecoration: "none",
        borderBottom: "3px solid #000000!important",
       },
  },

    MuiIcon: {
      fontSizeSmall: {
        fontSize: "12px !important"
      },
      fontSizeLarge: {
        fontSize: "42px !important"
      }
    },

    MuiButton: {
      root: {
        borderRadius: "0px",
        textTransform: "none",
        textAlign: 'center'
      },
      sizeSmall: {
        height: "25px !important",
        minWidth: "150px !important",
        fontSize: '14px',
      },
      sizeLarge: {
        height: "45px !important",
        minWidth: "312px !important",
        fontSize: '19px',
      },
      containedPrimary: {
        color: "#fafafa",
        backgroundColor: "#212121"
      },
      containedSecondary: {
        color: "#fafafa",
        backgroundColor: "#424242"
      },
      outlinedPrimary: {
        color: "#212121",
        border: "1px solid #212121",
        "&:hover": {
          color: "#ffffff",
          backgroundColor: "#212121"
        }
      }, 
      outlinedSecondary: {
        color: "#424242",
        border: "1px solid #424242"
      },
      textPrimary: {
        color: "#212121"
      },
      textSecondary: {
        color: "#424242"
      }
    },
    
    MuiTooltip: {
      tooltip: {
        backgroundColor: "#fffffff",
        borderRadius: 0,
        color: "#000000",
        minWidth: "240px !important",
        boxShadow: "rgba(0, 0, 0, 0.2) 0px 2px 8px 0px",
        borderColor: "rgb(102, 102, 102)",
        borderWidth: "0px",
        borderStyle: "solid" 
      }
    },

    MuiDialog: {
      paper: {
        width: '560px',
        height: '389px',
        borderRadius: '16px',
        backgroundColor: 'rgb(255, 255, 255)',
        backgroundSize: 'cover',
        boxShadow: '0px 4px 24px rgba(0,0,0,0.3)',
        border: '0px solid rgb(102, 102, 102)',
        color: '#000000',
        textAlign: 'center',
        lineHeight: "10px",
        mixBlendMode: 'normal',
        backgroundPosition: 'center center',
        textDecoration: 'none',
        backgroundRepeat: 'repeat'
      }
    },

    MuiDialogTitle: {
      root: {
        fontSize: '48px', 
        lineHeight: "56px",
        fontFamily: "Playfair Display, serif",  
        fontWeight: "regular" 
      },
    },

    MuiDialogContent: {
      root: {
        fontSize: '20px',
        lineHeight: '28px',
        fontWeight: 100,
        fontFamily: "Montserrat, sans-serif", 
      }
    },

    MuiDialogActions: {
      root: {
        textAlign: 'center'
      }
    },

    MuiTab: {
      root: {
        padding: 0,
        marrgin: 0
      },
      wrapper: {
        color: 'primary',
        textTransform: 'none',
        fontFamily: 'Montserrat',
        fontWeight: 'bold',
        height: 'auto !important',
        textAlign: 'left',
        width: '139px',
        fontSize: '14px',
        padding: '0px !important',
        flexDirection: 'row',
        alignItems: 'flex-start'
      }
    },


    MuiSwitch: {
      root: {
        width: "16px",
        '&$track': {
          height: "3px",
          opacity: 1.00
        },
      },
    },


    MuiSvgIcon: {
      root: {
        width: '25px',
        height: '25px',
        color: '#000000',
      },
    },

    MuiSelect: {
      select: {
        minWidth: 25,
      }
  },

    MuiPaper: {
      root: {
        textDecoration: "underline"
      },
    }


  }

});



export default function UXPinWrapper({ children }) {

  
  let icons = document.createElement('link');
  icons.setAttribute('href', 'https://fonts.googleapis.com/icon?family=Material+Icons');
  icons.setAttribute('rel', 'stylesheet');
  document.head.appendChild(icons);
  
  
  return <MuiThemeProvider theme={theme}>{children}</MuiThemeProvider>;
}
