import * as React from "react";
import Tabs from "../Tabs";
import Tab from "../../Tab/Tab";

export default (
  <Tabs
    indicatorColor="primary"
    textColor="primary"
    uxpId="1"
  >
    <Tab label="Find Color" uxpId="2" fullWidth />
    <Tab label="Color Matches" uxpId="3" fullWidth />
    <Tab label="My Projects" uxpId="4" fullWidth />
  </Tabs>
);
