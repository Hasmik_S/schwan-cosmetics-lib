import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import TabsM from "@material-ui/core/Tabs";




const initalState = {
  value: 0,
  userAction: false
}

const Tabs = (props) => {
  const [value, setValue] = useState(0)
  const [userAction, setUserAction] = useState(false)

  useEffect(() => {
    if (userAction === false) {
      setValue(initalState.value);
    }
  }, []);

  const handleChange = (event, value) => {
    setValue(value)
    setUserAction(true)
  }

  return (
    <TabsM {...props} value={value} onChange={handleChange}>{props.children}</TabsM>
  )
}

Tabs.propTypes = {
  /**
   * Callback fired when the component mounts.
   * This is useful when you want to trigger an action programmatically.
   * It currently only supports `updateIndicator()` action.
   *
   * @param {object} actions This object contains all possible actions
   * that can be triggered programmatically.
   */
  action: PropTypes.func,

  
  centered: PropTypes.bool,

  
  classes: PropTypes.object,

  
  className: PropTypes.string,

  
  component: PropTypes.string,

  
  fullWidth: PropTypes.bool,

  
  indicatorColor: PropTypes.oneOf(['secondary', 'primary']),

  /**
   * Callback fired when the value changes.
   *
   * @param {object} event The event source of the callback
   * @param {number} value We default to the index of the child
   */
  onChange: PropTypes.func,

  
  scrollable: PropTypes.bool,

  
  ScrollButtonComponent: PropTypes.node,

 
  scrollButtons: PropTypes.oneOf(['auto', 'on', 'off']),

  
  TabsIndicatorProps: PropTypes.object,

 
  textColor: PropTypes.oneOf(['secondary', 'primary', 'inherit']),

  
  theme: PropTypes.object,

  
  value: PropTypes.number,

  
  defaultValue: PropTypes.number,

  
  variant: PropTypes.oneOf(['standard', 'scrollable', 'fullWidth'])
};

export default Tabs;
