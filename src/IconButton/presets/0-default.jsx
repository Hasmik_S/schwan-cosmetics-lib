import * as React from 'react';
import IconButton from '../IconButton';
import Icon from '../../Icon/Icon';

export default (
  <IconButton uxpId="1" color="primary">
    <Icon uxpId="2">photo_library</Icon>
  </IconButton>
);