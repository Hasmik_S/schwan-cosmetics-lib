import React from "react";
import PropTypes from "prop-types";
import IconButtonM from "@material-ui/core/IconButton";
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { fontSize } from "@material-ui/system";


const theme = createMuiTheme({
  overrides: {
MuiIconButton: {
  root: {
    border: "1px solid #212121",
    borderRadius: 0,
    minHeight: "56px",
    minWidth: "56px",
    fontSize: 26
  },
  colorPrimary: {
    color: "#000000",
    backgroundColor: "#ffffff",
    "&:hover": {
      backgroundColor: "#000000",
      color: "#ffffff"
    },
    '@media (hover: none)': {
      backgroundColor: "#000000",
      color: "#ffffff"
    }
   },
  
   colorSecondary: {
    color: "#9e9e9e",
    backgroundColor: "#ffffff",
    border: "1px solid #9e9e9e",
    "&:hover": {
      backgroundColor: "#9e9e9e",
      color: "#ffffff",
      border: "1px solid #9e9e9e"
    },
    '@media (hover: none)': {
      backgroundColor: "#9e9e9e",
      color: "#ffffff",
      border: "1px solid #9e9e9e"
    }
   },

   sizeSmall: {
     minHeight: "48px",
     minWidth: "48px",
     fontSize: 24
   }
},
  }
})



function IconButton(props) {
  
  return ( <MuiThemeProvider theme={theme}>
  <IconButtonM {...props}>{props.children}</IconButtonM>
  </MuiThemeProvider>);
}

IconButton.propTypes = {

  onClick: PropTypes.func,
  
  size: PropTypes.oneOf(['small', 'medium']),

  color: PropTypes.oneOf(["default", "inherit", "primary", "secondary"]),
  
  disabled: PropTypes.bool,
  
  disableRipple: PropTypes.bool

};

export { IconButton as default };
