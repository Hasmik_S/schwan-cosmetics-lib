import * as React from 'react';
import Icon from '../Icon';

export default (
  <Icon uxpId="1"
  color="primary">
    home
  </Icon>
);