import React from "react";
import PropTypes from "prop-types";
import IconM from "@material-ui/core/Icon";



function Icon(props) {
  
  return <IconM {...props}>{props.children}</IconM>
}

Icon.propTypes = {
  
  children: PropTypes.node,

  /**
   * Override or extend the styles applied to the component.
   
  classes: PropTypes.object,
  className: PropTypes.string,
   */

  color: PropTypes.oneOf([
    "inherit",
    "primary",
    "secondary",
    "action",
    "error",
    "disabled"
  ]),
  
  fontSize: PropTypes.oneOf(["inherit", "default", "small", "large"])
};

export { Icon as default };
