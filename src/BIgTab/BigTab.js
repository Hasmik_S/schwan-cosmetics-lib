import PropTypes from "prop-types";
import React , {Component} from 'react';


class BigTab extends Component {
    
    render() {
    
        const buttonStyle = {
            border: '1px solid #000000', 
            backgroundColor: '#ffffff', 
            borderRadius: '16px', 
            width: '56px', 
            height: '32px', 
            display: 'flex',
            justifyContent: 'center',
            "&:hover": {
                color: '#ffffff', 
                backgroundColor: '#000000',
            },
        }

        const buttonContent = {
            borderRadius: '16px', 
            border: 'none', 
            backgroundColor: '#ffffff', 
            fontSize: '14px', 
            color: '#000000', 
            cursor: 'pointer',
            
        }

        return (
            <div style={buttonStyle}>
                <button  style={buttonContent}>
                    Text
                </button>
            </div>
        );
    }
}



BigTab.propTypes = {
 /**
   * This property isn't supported.
   * Use the `component` property if you need to change the children structure.
   */
  children: PropTypes.node,

  disabled: PropTypes.bool,

  icon: PropTypes.node,

  label: PropTypes.node,

  onChange: PropTypes.func,

  onClick: PropTypes.func,

  selected: PropTypes.bool,

  textColor: PropTypes.oneOf(['secondary', 'primary', 'inherit']),

  /**
   * You can provide your own value. Otherwise, we fallback to the child position index.
   */
  value: PropTypes.any 
};

export default BigTab;
