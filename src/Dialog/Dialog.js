import React, { useState } from "react";
import PropTypes from "prop-types";
import ButtonM from "../Button/Button";
import DialogM from "@material-ui/core/Dialog";
import DialogActionsM from "@material-ui/core/DialogActions";
import DialogContentM from "@material-ui/core/DialogContent";
import DialogContentTextM from "@material-ui/core/DialogContentText";
import DialogTitleM from "@material-ui/core/DialogTitle/DialogTitle";
import CheckboxM from '@material-ui/core/Checkbox/Checkbox';
import Image from '../Image/Image';
import del from './presets/delete-1.jpg';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';


const theme = createMuiTheme({
  overrides: {
    MuiButtonBase: {
      root: {
        borderRadius: '0%'
      }
    }
  }
});

const Dialog = () => {
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
 
  const handleClose = () => {
    setOpen(false);
  };

  return (
   
    <div >
      <ButtonM variant="outlined" color="primary" onClick={handleClickOpen} >
        Open
      </ButtonM>
      <DialogM open={open} onClose={handleClose} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description" >

         <DialogTitleM  > 
          <div  onClick={handleClose} style={{display: 'flex', justifyContent: 'flex-end' }}>
            <Image src={del} style={{width: '32px', height: '32px', lineHeight: '18px' }} />
          </div>

          Let's start.

          <div style={{display: 'flex', justifyContent: 'center'}}> 
            <p style={{backgroundColor: 'rgb(124, 51, 117)', backgroundPosition: 'center center', border: '0px solid rgb(102, 102, 102)', width: '40px', height: '3px'}} >
            </p>
          </div>
        </DialogTitleM>

        <DialogContentM >
          <DialogContentTextM color='primary' >
            How would you like to explore our Colors? Please choose one of these options.
          </DialogContentTextM>
          <div display='inline' >
          <MuiThemeProvider theme={theme}>
            <CheckboxM />
          </MuiThemeProvider>
            <span style={{fontFamily: 'Montserrat, sans-serif', fontSize: '14px', 
                        fontWeight: 100, fontStyle: 'thin'}} >
                          Entry (unselected)
            </span>
          </div>
        </DialogContentM>

        <DialogActionsM style={{display: 'flex', justifyContent: 'center' }}>
          <ButtonM onClick={handleClose} color='primary' variant='outlined' style={{borderRadius: 0, border: '1px solid #000000', lineHeight: '10px', width: '115px', height: '40px'}} >
            GOT IT!
          </ButtonM>
        </DialogActionsM>

      </DialogM>
    </div>
    
  );
}

Dialog.propTypes = {

    /**
     * Dialog children, usually the included sub-components.
     */
    children: PropTypes.node,

    /**
     * Override or extend the styles applied to the component.
     * See [CSS API](#css) below for more details.
     */
    classes: PropTypes.object,

    /**
     * @ignore
     */
    className: PropTypes.string,

    

    /**
     * Props applied to the [`Paper`](/api/paper/) element.
     */
    PaperProps: PropTypes.object,

};

export default Dialog;
