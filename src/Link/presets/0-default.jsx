import * as React from 'react';
import Link from '../Link';

export default (
  <Link 
  uxpId="1"
  color="primary"
  underline="always"
  variant="body1"
  font-weight="bold">
    Text
  </Link>
);