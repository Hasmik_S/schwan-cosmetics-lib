import React from "react";
import PropTypes from "prop-types";
import LinkM from "@material-ui/core/Link";


function Link(props) {
  return <LinkM {...props} style={{"display": "inline-block"}}>{props.children}</LinkM>
}



Link.propTypes = {

  onClick: PropTypes.func,
  /** 
  block: PropTypes.bool,
  */
  children: PropTypes.node,

  component: PropTypes.elementType,

  color: PropTypes.oneOf(['error', 'inherit', 'primary', 'secondary', 'textPrimary', 'textSecondary']),

  /**
   * `classes` property applied to the [`Typography`](/api/typography/) element.
  
  TypographyClasses: PropTypes.object,
  */
 
  underline: PropTypes.oneOf(['none', 'hover', 'always']),

  variant: PropTypes.string 
  
}

export { Link as default };
