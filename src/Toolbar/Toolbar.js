import React from "react";
import PropTypes from "prop-types";
import ToolbarM from "@material-ui/core/Toolbar";
import Tab from '../Tab/Tab';
import Tabs from '../Tabs/Tabs';




const Toolbar = () => {

  return (
  <div  > 
    <p style={{fontWeight: 'bold', fontSize: '26px', color: '#000000', paddingLeft: '60px', paddingTop: '7px'}}> Termin hinzufügen </p> <br/>
      <ToolbarM style={{ borderBottom: '1px solid rgb(230, 230, 230)', backgroundColor: '#ffffff', display: 'flex', justifyContent: 'center'}}>
        <Tabs indicatorColor="primary" textColor="primary" style={{display: 'inline'}}>
            <Tab color='primary' label="Termin auswählen" />
            <Tab color='primary' label="Voting erstellen" />
        </Tabs>
      </ToolbarM>
      </div>
  );
}  


Toolbar.propTypes = {
  /**
   * Toolbar children, usually a mixture of `IconButton`, `Button` and `Typography`.
  
  children: PropTypes.node,
  */

  /**
   * Override or extend the styles applied to the component.
   * See [CSS API](#css-api) below for more details.
   */
  classes: PropTypes.object,

  className: PropTypes.string,

  /**
   * If `true`, disables gutter padding.
   */
  disableGutters: PropTypes.bool,

  /**
   * The variant to use.
   */
  variant: PropTypes.oneOf(["regular", "dense"]),
  justifyContent: PropTypes.oneOf(["center", "flex-end", "flex-start", "space-around", "space-between", "space-evenly", "stretch"])
};

export default Toolbar;
