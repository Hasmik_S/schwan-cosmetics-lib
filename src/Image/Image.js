import React from 'react';
import PropTypes from 'prop-types';

const Image = (props) => {
  return <img {...props} />
}

Image.PropTypes = {
  src: PropTypes.string,
  alt: PropTypes.string,
  onClick: PropTypes.func,
  onClose: PropTypes.func
}



export default Image;
