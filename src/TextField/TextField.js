import React from "react";
import PropTypes from "prop-types";
import TextFieldM from "@material-ui/core/TextField";

function TextField(props) {
  return <TextFieldM {...props} />;
}

TextField.propTypes = {
  
  autoFocus: PropTypes.bool,

  /** 
  children: PropTypes.node,
  
  className: PropTypes.string,
  */

  defaultValue: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),

  disabled: PropTypes.bool,

  error: PropTypes.bool,
  /** 
  FormHelperTextProps: PropTypes.object,
  */

  fullWidth: PropTypes.bool,

  helperText: PropTypes.node,

  /**
   * The id of the `input` element.
   * Use that property to make `label` and `helperText` accessible for screen readers.
   */
  /** 
  id: PropTypes.string,

  InputLabelProps: PropTypes.object,

  /**
   * Properties applied to the `Input` element.
   
  InputProps: PropTypes.object,

  /**
   * Attributes applied to the native `input` element.
   
  inputProps: PropTypes.object,


  /**
   * Use that property to pass a ref callback to the native input component.
   
  inputRef: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.object
  ]),
  */

  label: PropTypes.node,

  margin: PropTypes.oneOf(["none", "dense", "normal"]),

  multiline: PropTypes.bool,

  /**
   * Name attribute of the `input` element.
   
  name: PropTypes.string,
  */

  onBlur: PropTypes.func,

  onChange: PropTypes.func,

  onFocus: PropTypes.func,

  placeholder: PropTypes.string,

  required: PropTypes.bool,

  /**
   * Number of rows to display when multiline option is set to true.
   */
  rows: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),

  /**
   * Maximum number of rows to display when multiline option is set to true.
   */
  rowsMax: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),

  /**
   * Render a `Select` element while passing the `Input` element to `Select` as `input` parameter.
   * If this option is set you must pass the options of the select as children.
   
  select: PropTypes.bool,
  SelectProps: PropTypes.object,
  */

  /**
   * Type attribute of the `Input` element. It should be a valid HTML5 input type.
   
  type: PropTypes.string,
  */

  /**
   * The value of the `Input` element, required for a controlled component.
   */
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.bool,
    PropTypes.arrayOf(
      PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.bool
      ])
    )
  ]),

  children: PropTypes.node,

  variant: PropTypes.oneOf(["standard", "outlined", "filled"])
};

export { TextField as default };
