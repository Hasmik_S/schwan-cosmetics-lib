import React from "react";
import PropTypes from "prop-types";
import ButtonM from "@material-ui/core/Button";




function Button(props) {
  return <ButtonM {...props} onClick={props.onClick}>{props.children}</ButtonM>
}


Button.propTypes = {

  onClick: PropTypes.func,
  
  children: PropTypes.node.isRequired,

 
  color: PropTypes.oneOf(['default', 'inherit', 'primary', 'secondary']),

  /** 
  component: PropTypes.elementType,
  */

  src: PropTypes.string,

  
  disabled: PropTypes.bool,

  
  disableFocusRipple: PropTypes.bool,

  
  disableRipple: PropTypes.bool,

  
  fullWidth: PropTypes.bool,

  
  href: PropTypes.string,

  
  size: PropTypes.oneOf(['small', 'medium', 'large']),

  
  variant: PropTypes.oneOf(['text', 'outlined', 'contained'])
};

export { Button as default };
