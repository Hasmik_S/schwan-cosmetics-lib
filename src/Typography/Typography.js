import React from "react";
import PropTypes from "prop-types";
import TypographyM from "@material-ui/core/Typography";


function Typography(props) {
  return <TypographyM {...props}>{props.children}</TypographyM>
}


Typography.propTypes = {
  
  align: PropTypes.oneOf([
    "inherit",
    "left",
    "center",
    "right"
  ]),

  children: PropTypes.node,

  /**
   * Override or extend the styles applied to the component.
   
  classes: PropTypes.object,

  className: PropTypes.string,
  */
 
  color: PropTypes.oneOf([
    "default",
    "error",
    "inherit",
    "primary",
    "secondary",
    'textPrimary', 
    'textSecondary'
  ]),

  component: PropTypes.elementType,

  display: PropTypes.oneOf(['initial', 'block', 'inline']),
 
  /**
   * If `true`, the text will not wrap, but instead will truncate with an ellipsis.
   
  noWrap: PropTypes.bool,
  */

  variant: PropTypes.oneOf([
    "h1",
    "h2",
    "h3",
    "h4",
    "h5",
    "h6",
    "subtitle1",
    "subtitle2",
    "body1",
    "body2",
    'body3',
    
  ]),
/** 
  other: PropTypes.oneOf([
    'reagular',
    'bold',
    'lighter'
  ])
  */
};

export { Typography as default };
