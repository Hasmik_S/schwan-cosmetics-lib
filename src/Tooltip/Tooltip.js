import React from "react";
import PropTypes from "prop-types";
import TooltipM from "@material-ui/core/Tooltip";

const Tooltip = (props) => {
    return (
        <TooltipM {...props} >
            {props.children ? props.children
            : <span>{props.title}</span>}
        </TooltipM>
    );
};

Tooltip.propTypes = {
    children: PropTypes.node,
    /**
     * Override or extend the styles applied to the component.
    
    classes: PropTypes.object,

    className: PropTypes.string,
     */
    
    component: PropTypes.elementType,

    title: PropTypes.node.isRequired,

    placement: PropTypes.oneOf([
      "bottom-end",
      "bottom-start",
      "bottom",
      "left-end",
      "left-start",
      "left",
      "right-end",
      "right-start",
      "right",
      "top-end",
      "top-start",
      "top"
    ])
};

Tooltip.defaultProps = {
  title: 'Content . . . ',
  placement: 'bottom'
}

export default Tooltip;
