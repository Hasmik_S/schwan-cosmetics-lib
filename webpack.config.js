const path = require("path");

module.exports = {
    output: {
        path: path.resolve(__dirname, "build"),
        filename: "bundle.js",
        publicPath: "/"
      },
  resolve: {
      modules: [__dirname, "node_modules"],
      extensions: ["*", ".js", ".jsx"]
  },
  devtool: "source-map",
  module: {
      rules: [
          {
              test: /\.js$/,
              loader: "babel-loader",
              exclude: /node_modules/
          },
          {
              test: /\.(png|jpe?g|gif)$/i,
              use: [
                  {
                      loader: "url-loader?name=images/[name].[ext]"
                  }
              ]
          },
          { 
            enforce: "pre", 
            test: /\.js$/, 
            loader: "source-map-loader" 
          },
          {
            test: /\.(eot|svg|ttf|woff|woff2)$/, 
            loader: 'url-loader?name=Fonts/files/[name].[ext]'
          }
      ]
  }
};
