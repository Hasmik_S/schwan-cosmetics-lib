module.exports = {
  components: {
    categories: [
      {
        name: "General",
        include: [
          "src/Button/Button.js",
          "src/Switch/Switch.js",
          "src/IconButton/IconButton.js",
          "src/Tooltip/Tooltip.js",
          "src/BigTab/BigTab.js",
          "src/Link/Link.js",
          "src/TextField/TextField.js",
          "src/Typography/Typography.js",
          "src/Icon/Icon.js",
          "src/Image/Image.js",
          "src/Dialog/Dialog.js",
          "src/Toolbar/Toolbar.js",
          'src/Header/Header.js',
        ]
      },
    ]
  },
  name: "Schwan Cosmetics Library"
};

/**
 * Currently not supported:
 * 'src/Menu/Menu.js',
 * 'src/ExpansionPanel/ExpansionPanel.js',
 * 'src/ExpansionPanelActions/ExpansionPanelActions.js',
 * 'src/ExpansionPanelDetails/ExpansionPanelDetails.js',
 * 'src/ExpansionPanelSummary/ExpansionPanelSummary.js'
 * 'src/Snackbar/Snackbar.js'
 * 'src/SnackbarContent/SnackbarContent.js'
 */
